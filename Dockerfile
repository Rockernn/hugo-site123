FROM nginx:alpine

COPY ./public /var/www/html
COPY ./nginx-docker/nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
